#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include "List.h"

using namespace std;

//Const was used here for convenient testing
void RemoveDups(const char* str)
{
	if (str)
	{
		string currentStr(str);

		auto first = begin(currentStr);
		auto last = first + 1;

		while (last != end(currentStr))
		{
			if (*first == *last) last++;						//Find a sequence
			if (last == end(currentStr) || *first != *last)		//Or end of string, or not equal symbols - remove sequence
			{
				currentStr.erase(first, last - 1);		//Leave one symbol
				last = ++first;
			}
		}

		cout << currentStr << endl;
	}
	else cout << "String is empty!" << endl;
}

//Start remove dups function test
void RemoveDupsTest()
{
	/*RemoveDups(nullptr);
	RemoveDups("abcdef");
	RemoveDups("aaabbccdeeffff");
	RemoveDups("abcdewwwwf");
	RemoveDups("AAA BBB AAA");*/
}

//Start serialization function test
void SerializationTest()
{
	const char* str[] = { "Hello", "My", "World!" };

	List* l = new List(str, 3);
	cout << "Before Serialization" << endl;
	l->ShowList();
	FILE* f = fopen("test.txt", "wb");

	if (f)
	{
		l->Serialize(f);
		fclose(f);
	}
	else cout << "Error open!!";

	f = fopen("test.txt", "rb");

	if (f)
	{
		l->Deserialize(f);
		fclose(f);
	}
	else cout << "Error open!!";

	cout << "After DEserialization" << endl;
	l->ShowList();
}

int main()
{
	/*
		All tests are made here!
	*/

	//RemoveDupsTest();
	//SerializationTest();
	return 0;
}