#include "List.h"
#include <iostream>

List::List(const char** strArr, int count)
{
    if (strArr)
    {
        for (int i = 0; i < count; i++)
        {
            PushBack(strArr[i]);
        }

        //Only for tests
        head->rand = tail;
    }
}

void List::PushBack(const char* str)
{
    if (str)
    {
        ListNode* newNode = new ListNode();
        newNode->data = std::string(str);
        newNode->rand = nullptr;
        PushBack(newNode);
    }
}

void List::PushBack(ListNode* newNode)
{
    if (newNode)
    {
        if (!head)      //Add first element
        {
            head = tail = newNode;
            head->prev = nullptr;
            head->next = nullptr;
            count = 1;
        }
        else
        {
            tail->next = newNode;
            newNode->prev = tail;
            newNode->next = nullptr;
            tail = newNode;
            count++;
        }
    }
}

void List::ClearList()
{
    while (head)
    {
        PopBack();
    }
}

void List::PopBack()
{
    ListNode* last = tail;
    
    if (head != tail)
    {
        ListNode* tailPrev = tail->prev;

        tailPrev->next = nullptr;
        tail = tailPrev;
    }
    else head = tail = nullptr;

    delete last;
    count--;
}

void List::ShowList()
{
    ListNode* temp = head;
    while (temp != nullptr)
    {
        if (!temp->rand)  cout << temp->data << endl;
        else
        {
            cout << temp->data << " with pointer on " << temp->rand->data << endl;
        }
        temp = temp->next;
    }
}

/*
Save list elements into binary file (save with rand pointers)
Format of file: count of elements; list nodes, rand pointers
*/
void List::Serialize(FILE* file)
{
    if (file)
    {
        int nodeSize = sizeof(ListNode);
        ListNode* temp = head;

        //Save list count to file
        fwrite(&count, sizeof(int), 1, file);

        //Write list
        while (temp != nullptr)
        {
            fwrite(temp, nodeSize, 1, file);
            temp = temp->next;
        }

        temp = head;

        //Write rand pointers
        while (temp != nullptr)
        {
            if (temp->rand)
            {
                fwrite(temp->rand, sizeof(*temp->rand), 1, file);
            }

            temp = temp->next;
        }
    }
}

/*
Create list by data in binary file (read with rand pointers)
Format of file: count of elements; list nodes, rand pointers
*/
void List::Deserialize(FILE* file)
{
    if (file)
    {
        //Clear before adding a new data
        ClearList();

        int nodeSize = sizeof(ListNode);
        ListNode* temp = nullptr;
        int prepareCount = 0;

        int nextNodePosition, nextRandPosition;

        //Get list count
        if (!feof(file))
        {
            fread(&prepareCount, sizeof(int), 1, file);
        }

        //Create list
        for (int i = 0; i < prepareCount; i++)
        {
            temp = new ListNode();

            nextNodePosition = i * nodeSize + sizeof(int);
            nextRandPosition = sizeof(int) + prepareCount * nodeSize + i * sizeof(*temp->rand);

            //Get new list node
            fseek(file, nextNodePosition, SEEK_SET);
            fread(temp, nodeSize, 1, file);

            temp->rand = new ListNode();

            //Get rand for list node, if node has a rand pointer
            fseek(file, nextRandPosition, SEEK_SET);
            fgetc(file);

            if (!feof(file))
            {
                fseek(file, nextRandPosition, SEEK_SET);
                fread(temp->rand, sizeof(*temp->rand), 1, file);
            }
            else temp->rand = nullptr;

            PushBack(temp);
        }
    }
}
