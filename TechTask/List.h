#pragma once
#include <string>

using namespace std;

struct ListNode {
    ListNode* prev;
    ListNode* next;

    //Random element pointer, or nullptr
    ListNode* rand;

    //Data stored in list
    std::string data;
};


class List
{
private:
    ListNode* head = nullptr;
    ListNode* tail = nullptr;
    int count = 0;

public:
    List(const char** strArr, int count);  

    //Add new element in back by string
    void PushBack(const char* str); 

    //Add new element in back by pointer
    void PushBack(ListNode* newNode);

    //Remove all elements
    void ClearList();

    //Remove last element
    void PopBack();

    //Show list on the screen
    void ShowList();
    
    /*
    Save list elements into binary file (save with rand pointers)
    Format of file: count of elements; list nodes, rand pointers
    */
    void Serialize(FILE* file);

    /*
    Create list by data in binary file (read with rand pointers)
    Format of file: count of elements; list nodes, rand pointers
    */
    void Deserialize(FILE* file);

     
};

